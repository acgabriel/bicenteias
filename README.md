# BICenteias

Repositório responsável por gerenciar o desenvolvimento das aplicações da meta 9 do projeto arbocontrol.

## Creating Enviroment

Para criar o ambiente utilizando anaconda, executar o comando a baixo.

```bash
$ conda env create -f bi-centeias.yml
$ conda activate bi-centeias
```

## Disable Enviroment

```bash
$ conda deactivate
```

## Update Enviroment

```bash
$ conda env export > bi-centeias.yml
$ conda list -e > requirements.txt 

```
