import pandas as pd
import os
import json

with open('metadados/schema.json') as f:
    global schema
    schema = json.load(f)

data_dengue = pd.read_excel('dados/sinam/dengue_2018_manaus.xlsx')

rules = schema[0]['rules']
for rule in rules:
    print(rule)

print(pd.notnull(data_dengue['ID_AGRAVO']))

